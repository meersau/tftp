package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"bitbucket.org/meersau/tftp"
	"bitbucket.org/meersau/tftp/clientadmin"
)

func main() {

	configfile := flag.String("configfile", "tftpd.cfg", "Config File")

	flag.Parse()

	srvConfig, err := loadConfigFile(*configfile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "config file fehler: %v\n", err)
		os.Exit(2)
	}

	stor, err := loadStorage(srvConfig.Storage)
	if err != nil {
		fmt.Fprintf(os.Stderr, "storage init error: %v\n", err)
		os.Exit(2)
	}

	cc, err := parseClientConfig(srvConfig)
	if err != nil {
		fmt.Fprintf(os.Stderr, "clients parse error: %v\n", err)
		os.Exit(2)
	}

	srv, err := tftp.NewServerWithStorage(srvConfig.Listen, stor)
	if err != nil {
		fmt.Fprintf(os.Stderr, "server init error: %v\n", err)
		os.Exit(2)
	}

	srv.ClientConfigs = cc

	if srvConfig.AdminWeb.Enable {
		ca := clientadmin.NewClientConifgAdmin(srvConfig.AdminWeb.Listen)
		ca.ClientConifgs = &srv.ClientConfigs
		fmt.Println("starte admin web")
		go ca.Start()
	}

	fmt.Println(srv.ClientConfigs)
	fmt.Printf("%#v\n", srvConfig)
	// srv.ClientConfigs.Delete("1.2.3.6/32")
	log.Fatal(srv.Serve())
}
