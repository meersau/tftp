package main

import (
	"fmt"
	"os"

	"bitbucket.org/meersau/tftp/clientconfig"
	"bitbucket.org/meersau/tftp/storage"
	"gopkg.in/yaml.v3"
)

type serverconfig struct {
	Listen        string         `yaml:"listen"`
	AdminWeb      adminWeb       `yaml:"adminweb"`
	Defaults      serverdefaults `yaml:"defaults"`
	Storage       storageconfig
	ClientConfigs []client `yaml:"clients"`
}

type serverdefaults struct {
	Blocked      bool
	WriteBlocked bool
	ReadBlocked  bool
}
type client struct {
	CIDR    string `yaml:"cidr"`
	Blocked bool   `yaml:"blocked"`
}

type adminWeb struct {
	Enable bool   `yaml:"enable"`
	Listen string `yaml:"listen"`
}

type storageconfig struct {
	Kind        string
	RootDir     string `yaml:"rootdir"`
	CreateFiles bool   `yaml:"createfiles"`
}

func getFlagConfig(rootDir string, createFiles bool, admin bool) serverconfig {
	flagconfig := serverconfig{}

	flagconfig.Storage.RootDir = rootDir
	flagconfig.Storage.CreateFiles = createFiles
	flagconfig.AdminWeb.Enable = admin

	return flagconfig
}

func mergeFlagConfigFile(flagConifg serverconfig, configfile string) (serverconfig, error) {
	cfgfileConfig, err := loadConfigFile(configfile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error load config: %v", err)
		return serverconfig{}, err
	}
	cfgfileConfig.AdminWeb.Enable = flagConifg.AdminWeb.Enable
	cfgfileConfig.Storage.RootDir = flagConifg.Storage.RootDir
	cfgfileConfig.Storage.CreateFiles = flagConifg.Storage.CreateFiles

	return cfgfileConfig, nil
}

func loadStorage(storageconifg storageconfig) (*storage.FileStorage, error) {

	return storage.NewFileStorage(storageconifg.RootDir, storageconifg.CreateFiles)
}

func loadConfigFile(filename string) (serverconfig, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return serverconfig{}, err
	}
	cfg := serverconfig{}
	if err := yaml.Unmarshal(data, &cfg); err != nil {
		return serverconfig{}, err
	}
	return cfg, nil
}

func parseClientConfig(srvconfig serverconfig) (clientconfig.ClientConfigs, error) {
	if srvconfig.ClientConfigs == nil {
		return nil, nil
	}
	clientConfigs := clientconfig.NewClientConfigs()

	for _, clientconifg := range srvconfig.ClientConfigs {
		if err := clientConfigs.Add(
			&clientconfig.ClientConfig{
				CIDR:    clientconifg.CIDR,
				Blocked: clientconifg.Blocked,
			}); err != nil {
			return nil, err
		}
	}

	return clientConfigs, nil
}
