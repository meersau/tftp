package tftp

import (
	"fmt"
)

type logger struct {
	level int
}

func (l *logger) setLevel(level int) {
	l.level = level
}

func (l logger) Printf(format string, v ...interface{}) {
	switch {
	case l.level == 0:
	case l.level > 5:
		fmt.Printf(format, v...)
	case l.level > 7:
		l.Debug(format, v...)
	}
}

func (l logger) Debug(format string, v ...interface{}) {
	if l.level >= 2 {
		fmt.Printf("[DEBUG] "+format, v...)
	}
}

func (l logger) Info(format string, v ...interface{}) {
	if l.level >= 1 {
		fmt.Printf("[INFO] "+format, v...)
	}
}

func (l logger) Error(format string, v ...interface{}) {
	fmt.Printf("[ERROR] "+format, v...)
}
