package storage

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

type FileStorage struct {
	rootdir     string
	createFiles bool
}

type FileStorageFile struct {
	storfile *os.File
}

func (fsf FileStorageFile) GetName() string {
	return fsf.storfile.Name()
}

func (fsf FileStorageFile) Close() error {
	return fsf.storfile.Close()
}

func (fsf FileStorageFile) ReadAt(p []byte, off int64) (n int, err error) {
	return fsf.storfile.ReadAt(p, off)
}

func (fsf FileStorageFile) Write(p []byte) (n int, err error) {
	return fsf.storfile.Write(p)
}

// NewFileStorage returns storage for filesystem
// rootdir must be an absolute path
// no . or .. allowed in path
func NewFileStorage(rootdir string, createFiles bool) (*FileStorage, error) {
	alldirs := rootdir
	if strings.Contains(alldirs, ".") || strings.Contains(alldirs, "..") {
		return nil, fmt.Errorf("rootdir must not contain relative paths")
	}
	if !filepath.IsAbs(rootdir) {
		return nil, fmt.Errorf("rootdir must be absolute")
	}

	cleanedRoot := filepath.Clean(rootdir)

	fileStorage := new(FileStorage)
	fileStorage.rootdir = cleanedRoot
	fileStorage.createFiles = createFiles

	return fileStorage, nil
}

func (fileStorage *FileStorage) CreateFile(name string) (StorageFile, error) {
	var stfile FileStorageFile
	var err error
	absoluteFilename := filepath.Join(fileStorage.rootdir, name)
	flag := os.O_WRONLY | os.O_TRUNC
	if fileStorage.createFiles {
		flag = flag | os.O_CREATE
	}
	stfile.storfile, err = os.OpenFile(absoluteFilename, flag, 0640)
	return stfile, err
}

func (fileStorage *FileStorage) GetFile(name string) (*bytes.Reader, error) {
	absoluteFilename := filepath.Join(fileStorage.rootdir, name)
	f, err := os.Open(absoluteFilename)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	content, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}

	return bytes.NewReader(content), nil
}

func (fileStorage *FileStorage) SaveFile(name string, data io.Reader) error {
	absoluteFilename := filepath.Join(fileStorage.rootdir, name)
	file, err := os.Create(absoluteFilename)
	if err != nil {
		return err
	}
	io.Copy(file, data)
	file.Close()
	return nil
}

func (fileStorage *FileStorage) Size(name string) uint16 {
	absoluteFilename := filepath.Join(fileStorage.rootdir, name)
	file, err := os.Stat(absoluteFilename)
	if err != nil {
		return 0
	}

	return uint16(file.Size())
}
