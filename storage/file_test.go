package storage

import (
	"bytes"
	"os"
	"testing"
)

func TestNewFileStorage(t *testing.T) {
	fstor, err := NewFileStorage("blah", false)
	if err == nil {
		t.Fatalf("should be error: %v", err)
	}
	fstor, err = NewFileStorage("/blah/../etc/hosts", false)
	if err == nil {
		t.Fatalf("shold be error: %v", err)
	}
	fstor, err = NewFileStorage("/blah/./etc/hosts", false)
	if err == nil {
		t.Fatalf("shold be error: %v", err)
	}

	fstor, err = NewFileStorage("/blah", false)
	if err != nil {
		t.Fatalf("should be no error: %v", err)
	}

	if fstor == nil {
		t.Fatalf("should not be bill")
	}
}

func TestFileStorage(t *testing.T) {
	dir, err := os.MkdirTemp("", "filestorage")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir) // clean up

	fileStorage, err := NewFileStorage(dir, false)
	if err != nil {
		t.Fatalf("no new File Storage: %v", err)
	}

	content := []byte("Hallo welt")
	contentSize := len(content)

	nocontent, err := fileStorage.GetFile("didnotexists")
	if nocontent != nil && err == nil {
		t.Errorf("content in nocontent: %v", nocontent)
	}

	if size := fileStorage.Size("didnotexists"); size != 0 {
		t.Error("size for non exists file")
	}

	contentBuffer := bytes.NewBuffer(content)
	if err := fileStorage.SaveFile("newfile", contentBuffer); err != nil {
		t.Errorf("save file error: %v", err)
	}

	if size := fileStorage.Size("newfile"); size != uint16(contentSize) {
		t.Errorf("file should have %d got %d", contentSize, size)
	}

	withContent, err := fileStorage.GetFile("newfile")
	if withContent == nil && err != nil {
		t.Errorf("got no file, err: %v", err)
	}

	readBuffer := make([]byte, contentSize)
	var n int
	n, err = withContent.ReadAt(readBuffer, 0)
	if err != nil {
		t.Errorf("read error %v, readed %d", err, n)
	}

	if string(readBuffer) != string(content) {
		t.Errorf("content not the same")
	}
}
