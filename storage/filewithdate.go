package storage

import (
	"bytes"
	"io"
	"time"
)

type FileStorageWithDate struct {
	fs         *FileStorage
	DateFormat string
	sep        string
}

func NewFileStorageWithDate(rootdir string, createFiles bool, dateformat string, sep string) (*FileStorageWithDate, error) {
	defaultDateformat := "2006-02-01"
	//check dateformat and if not set default
	if dateformat == "" {
		dateformat = defaultDateformat
	}

	fs, err := NewFileStorage(rootdir, createFiles)
	if err != nil {
		return nil, err
	}
	fsd := &FileStorageWithDate{}
	fsd.fs = fs
	fsd.DateFormat = dateformat
	fsd.sep = sep
	return fsd, nil
}

func (fsd *FileStorageWithDate) appendDateString(name string) string {
	datestring := time.Now().Format(fsd.DateFormat)
	return name + fsd.sep + datestring
}

func (fsd *FileStorageWithDate) CreateFile(name string) (StorageFile, error) {
	namewithdate := fsd.appendDateString(name)
	return fsd.fs.CreateFile(namewithdate)

}
func (fsd *FileStorageWithDate) GetFile(name string) (*bytes.Reader, error) {
	return fsd.fs.GetFile(name)
}

func (fsd *FileStorageWithDate) SaveFile(name string, data io.Reader) error {
	return fsd.fs.SaveFile(name, data)
}
func (fsd *FileStorageWithDate) Size(name string) uint16 {
	return fsd.fs.Size(name)
}
