package storage

import (
	"bytes"
	"io"
)

type Storage interface {
	CreateFile(name string) (StorageFile, error)
	GetFile(name string) (*bytes.Reader, error)
	SaveFile(name string, data io.Reader) error
	Size(name string) uint16
}

type StorageFile interface {
	io.Writer
	io.Closer
	io.ReaderAt
	GetName() string
}

type DevNullStorage struct {
	//file DevNullFile
}

type DevNullFile struct {
	name string
	file bytes.Buffer
}

func (df DevNullFile) Close() error {
	return nil
}

func (df DevNullFile) Write(p []byte) (n int, err error) {
	return df.file.Write(p)
}

func (df DevNullFile) ReadAt(p []byte, off int64) (n int, err error) {
	return bytes.NewReader([]byte("Null Reader")).ReadAt(p, off)

}

func (df DevNullFile) GetName() string {
	return df.name
}

func NewDevNullStorage() *DevNullStorage {
	return &DevNullStorage{}
}

func (dv *DevNullStorage) CreateFile(name string) (StorageFile, error) {

	return DevNullFile{name: name}, nil
}

func (dv *DevNullStorage) GetFile(name string) (*bytes.Reader, error) {
	return bytes.NewReader([]byte("Null Reader Text")), nil
}

func (dv *DevNullStorage) SaveFile(name string, data io.Reader) error {
	return nil
}

func (dv *DevNullStorage) Size(name string) uint16 {
	return 0
}
