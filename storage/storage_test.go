package storage

import (
	"bytes"
	"testing"
)

func TestNewDevNullStorage(t *testing.T) {
	filename := "afile"
	devNullStorage := NewDevNullStorage()
	if devNullStorage == nil {
		t.Error("got nil Storage")
	}
	reader, err := devNullStorage.GetFile(filename)
	if reader == nil {
		t.Errorf("got no io.Reader:% v", reader)
	}
	if err != nil {
		t.Errorf("got error: %v", err)
	}

	w := bytes.NewBuffer(make([]byte, 10))
	if err := devNullStorage.SaveFile(filename, w); err != nil {
		t.Errorf("got err %v", err)
	}

	size := devNullStorage.Size(filename)
	if size != 0 {
		t.Errorf("size got %d, want 0", size)
	}

}
