package clientconfig

import (
	"log"
	"testing"
)

func TestClientAdd(t *testing.T) {
	cc := NewClientConfigs()

	if err := cc.Add(&ClientConfig{
		CIDR: "1.2.3.4",
	}); err == nil {
		t.Fatal("client config must have parseable CIDR")
	}

	if err := cc.Add(&ClientConfig{
		CIDR: "1.2.3.4/32",
	}); err != nil {
		t.Fatal("CIDR sould be parseable")
	}

	if err := cc.Add(&ClientConfig{
		CIDR: "1.2.3.4/32",
	}); err == nil {
		t.Fatal("client should not double added")
	}

	if notaddedclient := cc.Get("2.3.4.5"); notaddedclient != nil {
		log.Fatalf("client not in configs, but found: %v", notaddedclient)
	}

	if invaliedip := cc.Get("keineipadresse"); invaliedip != nil {
		log.Fatalf("invalid ip should not get config: %v", invaliedip)
	}

	addedClient := cc.Get("1.2.3.4")
	if addedClient == nil {
		log.Fatalf("added client should be getable: %v", addedClient)
	}
}
