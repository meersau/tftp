package clientconfig

import (
	"fmt"
	"net"
	"slices"
	"strconv"
	"strings"
)

type ClientConfig struct {
	CIDR         string
	IPNet        *net.IPNet
	Blocked      bool
	ReadBlocked  bool
	WriteBlocked bool
	ReadDir      string
	WriteDir     string
}

type ClientConfigs map[int][]*ClientConfig

func NewClientConfigs() ClientConfigs {
	return make(ClientConfigs)
}

func (cc ClientConfigs) Add(clientConfig *ClientConfig) error {
	if cc == nil {
		return fmt.Errorf("no clientconifg")
	}
	ip, net, err := net.ParseCIDR(clientConfig.CIDR)
	if err != nil {
		return err
	}
	clientConfig.IPNet = net

	parts := strings.Split(clientConfig.CIDR, "/")
	cidr, err := strconv.Atoi(parts[1])
	if err != nil {
		return err
	}

	if cc.isInCIDR(cidr, ip) {
		return fmt.Errorf("client %v exists", clientConfig)
	}

	cc[cidr] = append(cc[cidr], clientConfig)
	return nil
}

func (cc ClientConfigs) isInCIDR(cidr int, ip net.IP) bool {
	cidrclients := cc[cidr]
	for _, client := range cidrclients {
		if client.IPNet.Contains(ip) {
			return true
		}
	}
	return false
}

func (cc ClientConfigs) Get(ip string) *ClientConfig {
	clientIP := net.ParseIP(ip)

	if clientIP == nil {
		return nil
	}

	for i := 128; i > 0; i-- {
		clients := cc[i]
		for _, client := range clients {
			if client.IPNet.Contains(clientIP) {
				return client
			}
		}
	}
	return nil
}

func (cc ClientConfigs) GetAll() []*ClientConfig {
	configs := make([]*ClientConfig, 0)
	for _, c := range cc {
		configs = append(configs, c...)
	}
	return configs
}
func (cc ClientConfigs) Delete(cidr string) error {
	_, _, err := net.ParseCIDR(cidr)
	if err != nil {
		return err
	}
	parts := strings.Split(cidr, "/")
	masklength, _ := strconv.Atoi(parts[1])

	todel := -1
	for i, client := range cc[masklength] {
		if client.CIDR == cidr {
			todel = i
		}
	}
	cc[masklength] = slices.Delete(cc[masklength], todel, todel+1)
	return nil
}
