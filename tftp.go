// Package tftp implents a TFTP server RFC1350
// and also RFC2348 and RFC2348
// It also has a simple ACL for clients to allow
// or disallow Read/Write requests and give an
// own directory for Read/Write request
// rfc7440 to be implemented
package tftp

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"math/rand"
	"net"
	"strconv"
	"time"
)

const (
	// Opcodes
	opcodeRRQ  uint16 = 1
	opcodeWRQ  uint16 = 2
	opcodeDATA uint16 = 3
	opcodeACK  uint16 = 4
	opcodeERR  uint16 = 5
	opcodeOACK uint16 = 6
)

func opcodeString(opcode uint16) string {
	switch opcode {
	case opcodeRRQ:
		return "RRQ"
	case opcodeWRQ:
		return "WRQ"
	case opcodeDATA:
		return "DATA"
	case opcodeACK:
		return "ACK"
	case opcodeERR:
		return "ERR"
	case opcodeOACK:
		return "OACK"
	default:
		return "Unknown"
	}
}

// Client holds info from one client
type Client struct {
	Addr        *net.UDPAddr
	Conn        *net.UDPConn
	mode        string
	BlockSize   int
	TimeOut     int
	Tsize       uint16
	BlockNumber uint16
	WindowSize  int // RFC7440
	Options     map[string][]byte
	Databuf     []byte
	log         logger
}

func newClient(caddr *net.UDPAddr) *Client {
	if caddr == nil {
		return nil
	}
	c := new(Client)
	c.Addr = caddr
	c.BlockSize = 512
	c.TimeOut = 5
	c.WindowSize = 1 // RFC7440 Windowsize 1 == RFC1350
	c.Options = make(map[string][]byte)

	tid := strconv.Itoa(rand.Intn(10000) + 20000)

	a, _ := net.ResolveUDPAddr("udp4", ":"+tid)
	/* Gives error?!
	if err != nil {
		return err
	}*/
	var err error
	c.Conn, err = net.ListenUDP("udp4", a)
	if err != nil {
		return nil
	}
	return c
}

// Close closes UDP Connection
func (c *Client) Close() {
	c.Conn.Close()
}

// getPacket reads packet from UDP socket
func (c *Client) getPacket() ([]byte, error) {
	buf := make([]byte, 1024)

	c.Conn.SetReadDeadline(time.Now().Add(time.Second * time.Duration(c.TimeOut)))
	// Retrun or check here of Timeout()
	n, addr, err := c.Conn.ReadFromUDP(buf)
	if err != nil {
		if e, ok := err.(net.Error); ok && e.Timeout() {
			c.log.Info("[%s] getPacket Timeout")
			return buf, err
		}
		return buf, err
	}
	c.log.Debug("got: %d bytes from %v\n", n, addr)
	buf = append(make([]byte, 0), buf[:n]...)
	return buf, nil
}

// NewTransfer starts a new transfer from client WRQ
func (c *Client) NewTransfer(mode string) error {

	c.doOACK()
	c.mode = mode
	if err := c.handleDATA(); err != nil {
		return err
	}

	if c.mode == "netascii" {
		c.Databuf = bytes.Replace(c.Databuf, []byte("\r"), []byte(""), -1)
	}

	return nil
}

// SendFile sends file to client
// answer to RRQ Request
// why mode ???func (c *Client) SendFile(filename, mode string) error {
func (c *Client) SendFile(file io.ReaderAt) error {

	if c.BlockSize != 512 {
		p, _ := c.getPacket()
		if err := c.checkACK(p); err != nil {
			return err
		}
	}
	retry := 3

SENDLOOP:
	for {
		err := c.sendData(file)
		if err != nil {
			if e, ok := err.(net.Error); ok && e.Timeout() && retry > 0 {
				time.Sleep(time.Millisecond * time.Duration(rand.Intn(10)))
				retry--
				continue SENDLOOP
			}

			if err == io.EOF {
				return nil
			}

		}
		// RFC7740 - Check ACK after Windowsize
		if c.BlockNumber&uint16(c.WindowSize) == 0 {
			p, _ := c.getPacket()
			if err := c.checkACK(p); err != nil {
				return err
			}
		}
	}

}

func (c *Client) checkACK(buf []byte) error {
	opcode := binary.BigEndian.Uint16(buf)
	switch opcode {
	case opcodeACK:
		back := binary.BigEndian.Uint16(buf[2:])
		if c.BlockSize != 512 && back == 0 {
			return nil
		}
		if c.BlockNumber != back {
			return fmt.Errorf("false ACK# from client: %d should be %d", back, c.BlockNumber)
		}
		return nil
	case opcodeERR:
		return fmt.Errorf("client sends ERR")
	}
	return fmt.Errorf("got opcode: %d (%s), should be %d (%s) or %d (%s)", opcode, opcodeString(opcode),
		opcodeACK, opcodeString(opcodeACK), opcodeERR, opcodeString(opcodeERR))
}

func (c *Client) sendData(r io.ReaderAt) error {
	var lastblock bool

	block := make([]byte, c.BlockSize)

	// Lese at block# * blocksize
	n, err := r.ReadAt(block, int64(c.BlockNumber*uint16(c.BlockSize)))
	if n < c.BlockSize && err == io.EOF {
		lastblock = true
		block = append(make([]byte, 0), block[:n]...)
	}
	if err != nil && err != io.EOF {
		return err
	}
	c.BlockNumber++
	pack := make([]byte, 4)
	binary.BigEndian.PutUint16(pack, opcodeDATA)
	binary.BigEndian.PutUint16(pack[2:], c.BlockNumber)
	pack = append(pack, block...)

	c.log.Debug("send block# %d  len: %d last: %v\n", c.BlockNumber, len(pack), lastblock)
	if err := c.send(pack); err != nil {
		c.BlockNumber--
		return err
	}
	if lastblock {
		return io.EOF
	}
	return nil
}

func (c *Client) send(p []byte) error {
	// no timeout needed?
	//c.Conn.SetWriteDeadline(time.Now().Add(time.Second * time.Duration(c.TimeOut)))

	if _, err := c.Conn.WriteToUDP(p, c.Addr); err != nil {
		return err
	}
	return nil
}

// handleDATA recursive DATA packet handling
func (c *Client) handleDATA() error {

	buf, err := c.getPacket()
	if err != nil {
		return fmt.Errorf("no packet from client: %v", err)
	}

	opcode := binary.BigEndian.Uint16(buf)
	c.log.Debug("got opcode: %d (%s)\n", opcode, opcodeString(opcode))

	switch opcode {

	case opcodeDATA:
		block := binary.BigEndian.Uint16(buf[2:])
		c.log.Debug("[%s] got block#: %d\n", c.Addr, block)
		c.log.Debug("[%s] client has block#: %d\n", c.Addr, c.BlockNumber)

		if block != c.BlockNumber+1 {
			return fmt.Errorf("got block# %d should be: %d", block, c.BlockNumber)
		}

		c.Databuf = append(c.Databuf, buf[4:]...)
		// c.log.Debug("[%s] client databuf after append: %d\n", c.Addr, len(c.Databuf))
		c.BlockNumber++
		// RFC7740 - ACK only after Windowsize
		if c.BlockNumber%uint16(c.WindowSize) == 0 {
			c.sendACK()
		}
		if len(buf)-4 < c.BlockSize {
			c.log.Debug("[%s] client databuf: %d\n", c.Addr, len(c.Databuf))
			c.log.Debug("[%s] lastblock\n", c.Addr)
			return nil
		}

		if err := c.handleDATA(); err != nil {
			return err
		}
	default:
		return fmt.Errorf("client send opcode %d (%s) packet during DATA", opcode, opcodeString(opcode))

	}
	return nil
}

func (c *Client) setblksize(blksize []byte) {
	bs, _ := strconv.Atoi(string(blksize))
	if bs > 7 && bs < 65465 {
		c.BlockSize = bs
	} else {
		c.BlockSize = 512
	}
}

func (c *Client) setTimeout(timeout []byte) {
	to, _ := strconv.Atoi(string(timeout))
	if to < 1 || to > 255 {
		c.TimeOut = 5
	} else {
		c.TimeOut = to
	}
}

func (c *Client) setWindowSize(windowsize []byte) {
	// The base-10 ASCII string representation of the number of blocks in
	// a window.  The valid values range MUST be between 1 and 65535
	// blocks, inclusive.
	ws, _ := strconv.Atoi(string(windowsize))
	if ws < 1 || ws > 65535 {
		c.WindowSize = 1
	} else {
		c.WindowSize = ws
	}
}

func (c *Client) tsize(tsize []byte) {
	ts, err := strconv.ParseUint(string(tsize), 10, 16)
	if err != nil {
		c.Tsize = 0
		return
	}
	c.Tsize = uint16(ts)
}

func (c *Client) parseOptions(fields [][]byte) {
	known := make(map[string]func([]byte))
	known["blksize"] = c.setblksize
	known["timeout"] = c.setTimeout
	known["tsize"] = c.tsize
	known["windowsize"] = c.setWindowSize

	for i, v := range fields {
		vs := string(bytes.ToLower(v))
		if f, ok := known[vs]; ok {
			c.log.Debug("[%s] found option %s\n", c.Addr, vs)
			f(fields[i+1])
			if len(fields) >= i+1 {
				c.Options[string(v)] = fields[i+1]
			}
		}
		c.log.Debug("[%s] unknown option %s\n", c.Addr, vs)
	}

	if len(c.Options) == 0 {
		c.log.Debug("[%s] NO OPTIONS\n", c.Addr)
	}
}

// sendErr
//
//	Value     Meaning
//
//	0         Not defined, see error message (if any).
//	1         File not found.
//	2         Access violation.
//	3         Disk full or allocation exceeded.
//	4         Illegal TFTP operation.
//	5         Unknown transfer ID.
//	6         File already exists.
//	7         No such user.
func (c *Client) sendERR(eno uint16, msg ...string) error {
	pack := make([]byte, 4)
	binary.BigEndian.PutUint16(pack, opcodeERR)
	binary.BigEndian.PutUint16(pack[2:], eno)
	for _, v := range msg {
		pack = append(pack, v...)
		break
	}
	pack = append(pack, 0x00)
	return c.send(pack)

}

// doOACK sends an OACK if any options are set
// or an ACK if not
func (c *Client) doOACK() {

	if len(c.Options) > 0 {
		c.log.Debug("[%s]send OACK\n", c.Addr)
		pack := make([]byte, 2)
		binary.BigEndian.PutUint16(pack, opcodeOACK)
		for k, v := range c.Options {
			pack = append(pack, k...)
			pack = append(pack, 0x00)
			pack = append(pack, v...)
			pack = append(pack, 0x00)
		}
		c.send(pack)
	} else {
		c.log.Debug("[%s] NO OPTIONS SET send ACK\n", c.Addr)
		c.sendACK()
	}

}

// sendACK sends ACK with Blocknumber
func (c *Client) sendACK() error {
	pack := make([]byte, 4)
	binary.BigEndian.PutUint16(pack, opcodeACK)
	binary.BigEndian.PutUint16(pack[2:], c.BlockNumber)
	return c.send(pack)
}
