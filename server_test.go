package tftp

import (
	"fmt"
	"log"
	"testing"

	"bitbucket.org/meersau/tftp/storage"
)

var ts *Server

func init() {
	var err error
	ts, err = NewServerWithStorage("127.0.0.1:4545", storage.NewDevNullStorage())
	if err != nil {
		log.Printf("can't start server: %v", err)
		return
	}
}

func TestIsWriteOrReadOpcode(t *testing.T) {
	tests := []struct {
		name  string
		input uint16
		want  bool
	}{
		{name: "opcode 1", input: 1, want: true},
		{name: "opcode 2", input: 2, want: true},
		{name: "opcode 0", input: 0, want: false},
		{name: "opcode 18", input: 18, want: false},
	}

	for _, tc := range tests {
		got := isWriteOrReadOpcode(tc.input)
		if got != tc.want {
			t.Fatalf("%s: expected: %v, got: %v", tc.name, tc.want, got)

		}
	}
}

func TestIsModeSupported(t *testing.T) {
	tests := []struct {
		name string
		mode string
		want error
	}{
		{name: "mode netascii", mode: "netascii", want: nil},
		{name: "mode NetAscii", mode: "NetAscii", want: nil},
		{name: "mode binary", mode: "binary", want: nil},
		{name: "mode octet", mode: "octet", want: nil},
		{name: "mode gh", mode: "gh", want: fmt.Errorf("mode %s not implemented", "gh")},
	}
	for _, tc := range tests {
		got := isModeSupported(tc.mode)
		if got == nil {
			if !(got == tc.want) {
				log.Fatalf("%s: expected: \"%v\", got \"%v\"", tc.name, tc.want, got)

			}
		} else {
			if !(got.Error() == tc.want.Error()) {
				log.Fatalf("%s: expected: \"%v\", got \"%v\"", tc.name, tc.want, got)
			}

		}
	}
}

/*
func TestReadFromServer(t *testing.T) {
	client, err := net.Dial("upd4","127.0.0.1:4545")
	if err != nil {
		t.Errorf("can't open connetion: %v", err)
		return
	}
	defer client.Close()

	tt := []struct {
        test    string
        payload []byte
        want    []byte
    }{
        {
            "Sending false OP",
            []byte("0\N"),
            []byte("")
        },
        {
            "Sending right OP",
            []byte("goodbye world\n"),
            []byte("Request received: goodbye world")
        },
    }
} */

/* func TestHandleFirstPacket(t *testing.T) {
	buf := make([]byte, 2)
	binary.BigEndian.PutUint16(buf, 3)

	caddr, _ := net.ResolveUDPAddr("udp4", "127.0.0.1")

	if err := ts.handleFirstPacket(buf, caddr); err == nil {
		t.Errorf("got no error %v", err)
	}

	binary.BigEndian.PutUint16(buf, 1)
	if err := ts.handleFirstPacket(buf, caddr); err != nil {
		t.Errorf("got no error %v", err)
	}

}
*/
