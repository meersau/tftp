package clientadmin

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"

	"bitbucket.org/meersau/tftp/clientconfig"
)

type clientconfigadmin struct {
	listen        string
	ClientConifgs *clientconfig.ClientConfigs
	templates     *template.Template
}

func NewClientConifgAdmin(listen string) clientconfigadmin {

	t, err := NewUI()
	if err != nil {
		fmt.Println("t: ", err)
	}

	return clientconfigadmin{
		listen:    listen,
		templates: t,
	}
}

func (ca clientconfigadmin) Start() error {
	http.HandleFunc("/add", ca.AddClientConfig)
	http.HandleFunc("/list", ca.List)
	http.HandleFunc("/admin", ca.Admin)
	http.HandleFunc("/delete", ca.Delete)

	fmt.Println("Start admin")
	return http.ListenAndServe(ca.listen, nil)

}

func (ca clientconfigadmin) Admin(w http.ResponseWriter, r *http.Request) {

	tdata := struct {
		Clients []*clientconfig.ClientConfig
	}{}

	tdata.Clients = ca.ClientConifgs.GetAll()

	if err := ca.templates.Execute(w, tdata); err != nil {
		fmt.Println("Admin err: ", err)
	}
}

func (ca clientconfigadmin) AddClientConfig(w http.ResponseWriter, r *http.Request) {
	clientconifg := clientconfig.ClientConfig{}

	if err := json.NewDecoder(r.Body).Decode(&clientconifg); err != nil {
		http.Error(w, "Decode failed", http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	if err := ca.ClientConifgs.Add(&clientconifg); err != nil {
		http.Error(w, "fehler", http.StatusInternalServerError)
		return
	}

}

func (ca clientconfigadmin) Delete(w http.ResponseWriter, r *http.Request) {
	clientconifg := clientconfig.ClientConfig{}

	if err := json.NewDecoder(r.Body).Decode(&clientconifg); err != nil {
		fmt.Println(err)

		http.Error(w, "Decode failed", http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	if err := ca.ClientConifgs.Delete(clientconifg.CIDR); err != nil {
		http.Error(w, "fehler", http.StatusInternalServerError)
		return
	}
}

func (ca clientconfigadmin) List(w http.ResponseWriter, r *http.Request) {
	cc := ca.ClientConifgs.GetAll()
	json.NewEncoder(w).Encode(cc)

}
