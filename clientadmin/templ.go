package clientadmin

import (
	"embed"
	_ "embed"
	"fmt"
	"html/template"
)

// this comment will load content of all the files within 'ui' directory
//
//go:embed templates/*
var uiFS embed.FS

func NewUI() (*template.Template, error) {
	tpl, err := template.ParseFS(uiFS, "templates/*.tmpl")
	if err != nil {
		return nil, fmt.Errorf("parsing the template from embedded filesystem, %w", err)
	}

	return tpl, nil
}
