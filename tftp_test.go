package tftp

import (
	"net"
	"testing"
)

func TestNewClient(t *testing.T) {
	client := newClient(nil)
	if client != nil {
		t.Fatal("client should be nil ")
	}
	ua, _ := net.ResolveUDPAddr("udp4", "127.0.0.1:4566")
	client = newClient(ua)
	if client == nil {
		t.Fatal("client should not be nil")
	}

}

func TestOpcodeString(t *testing.T) {
	tests := []struct {
		opcode uint16
		is     string
	}{
		{is: "RRQ", opcode: 1},
		{is: "WRQ", opcode: 2},
		{is: "DATA", opcode: 3},
		{is: "ACK", opcode: 4},
		{is: "ERR", opcode: 5},
		{is: "OACK", opcode: 6},
		{is: "Unknown", opcode: 0},
		{is: "Unknown", opcode: 7},
	}

	for _, tc := range tests {
		got := opcodeString(tc.opcode)
		if got != tc.is {
			t.Errorf("want %s, got %s", tc.is, got)
		}
	}

}

func TestSetWindowSize(t *testing.T) {
	c := new(Client)
	c.setWindowSize([]byte{'a'})
	if c.WindowSize != 1 {
		t.Errorf("shoud be 1, got %d", c.WindowSize)
	}

	c.setWindowSize([]byte{'1', '2'})
	if c.WindowSize != 12 {
		t.Errorf("shoud be 12, got %d", c.WindowSize)
	}

	c.setWindowSize([]byte{'7', '0', '0', '0', '0', '0', '0', '0', '0'})
	if c.WindowSize != 1 {
		t.Errorf("shoud be 1, got %d", c.WindowSize)
	}

}

func TestBlksizeOption(t *testing.T) {
	c := new(Client)
	c.setblksize([]byte{'5'})
	if c.BlockSize != 512 {
		t.Errorf("Should be 512, got: %v", c.BlockSize)
	}
	c.setblksize([]byte{'5', '1', '6'})
	if c.BlockSize != 516 {
		t.Errorf("Should be 516, got: %v", c.BlockSize)
	}
	c.setblksize([]byte{'6', '5', '4', '6', '5'})
	if c.BlockSize != 512 {
		t.Errorf("Should be 512, got: %v", c.BlockSize)
	}
	c.setblksize([]byte{'a', 'b', 'c'})
	if c.BlockSize != 512 {
		t.Errorf("Should be 512, got: %v", c.BlockSize)
	}

}

func TestSetTimeout(t *testing.T) {
	c := new(Client)

	tests := []struct {
		name  string
		input []byte
		want  int
	}{
		{name: "timeout in range", input: []byte("5"), want: 5},
		{name: "timeout out of range", input: []byte("456"), want: 5},
		{name: "timeout is string", input: []byte("hallo"), want: 5},
		{name: "timeout is minus", input: []byte("-1"), want: 5},
	}

	for _, tc := range tests {
		c.setTimeout(tc.input)
		if c.TimeOut != tc.want {
			t.Errorf("should be %d, got %v", tc.want, c.TimeOut)
		}
	}

}

func TestTsize(t *testing.T) {
	c := new(Client)

	tests := []struct {
		name  string
		input []byte
		want  uint16
	}{
		{name: "tsize set", input: []byte("5"), want: 5},
		{name: "tsize minus", input: []byte("-5"), want: 0},
		{name: "tsize greater type", input: []byte("6553589"), want: 0},
	}

	for _, tc := range tests {
		c.tsize(tc.input)
		if c.Tsize != tc.want {
			t.Errorf("%s: should be %d, got %v", tc.name, tc.want, c.Tsize)
		}
	}

}

func TestParseOptions(t *testing.T) {
	ua, _ := net.ResolveUDPAddr("udp4", "127.0.0.1:4566")
	client := newClient(ua)

	options := make([][]byte, 0)
	client.parseOptions(options)

	if len(client.Options) > len(options) {
		t.Errorf("client has %d options should have %d", len(client.Options), len(options))
	}

	options = append(options, []byte("tsize"))
	options = append(options, []byte("12"))
	options = append(options, []byte("windowsize"))
	options = append(options, []byte("34"))
	client.parseOptions(options)
	if len(client.Options) > len(options) {
		t.Errorf("client has %d options should have %d", len(client.Options), len(options))
	}
	if client.Tsize != 12 {
		t.Errorf("client tsize should be 12")
	}
	if _, ok := client.Options["tsize"]; !ok {
		t.Errorf("client option map not set")
	}
	if client.WindowSize != 34 {
		t.Errorf("client windowsize should be 34")
	}

}
