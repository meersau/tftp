package tftp

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"net"
	"strconv"
	"strings"
	"sync"

	"bitbucket.org/meersau/tftp/clientconfig"
	"bitbucket.org/meersau/tftp/storage"
)

// Server is the TFTP Server
type Server struct {
	con           *net.UDPConn
	storage       storage.Storage
	ClientConfigs clientconfig.ClientConfigs
	Defaults      ServerDefaults
	wg            sync.WaitGroup
	log           logger
}

type ServerDefaults struct {
	Blocked      bool
	WriteBlocked bool
	ReadBlocked  bool
}

func NewServerWithStorage(addr string, storage storage.Storage) (*Server, error) {
	srv := new(Server)
	srv.storage = storage

	srv.log.setLevel(7)
	uaddr, err := net.ResolveUDPAddr("udp", addr)
	if err != nil {
		return srv, err
	}
	srv.con, err = net.ListenUDP("udp", uaddr)
	if err != nil {
		return srv, err
	}

	return srv, nil
}

func NewServer(address string) (*Server, error) {
	return NewServerWithStorage(address, storage.NewDevNullStorage())
}

func isFileNameAllowed(filename string) bool {
	return !strings.Contains(filename, "..")
}

func (ts *Server) IsIPBlocked(ip string) bool {
	if ts.ClientConfigs == nil {
		ts.log.Debug("Keine Client Configs, Server Default")
		return ts.Defaults.Blocked
	}
	client := ts.ClientConfigs.Get(ip)
	if client == nil {
		ts.log.Debug("Kein Client gefunden, Server Default")
		return ts.Defaults.Blocked
	}
	return client.Blocked
}

func (ts *Server) AllowedOPCodeforClient(ip string, opcode uint16) bool {
	readblocked := ts.Defaults.ReadBlocked
	writeBlocked := ts.Defaults.WriteBlocked

	if ts.ClientConfigs != nil {
		client := ts.ClientConfigs.Get(ip)
		if client != nil {
			readblocked = client.ReadBlocked
			writeBlocked = client.WriteBlocked
		}
	}
	switch opcode {
	case opcodeRRQ:
		return !readblocked
	case opcodeWRQ:
		return !writeBlocked
	}
	return false
}

// Serve opens port and waits for connections
func (ts *Server) Serve() error {

	for {
		ts.log.Info("waiting for connection %v\n", ts.con.LocalAddr())
		buf := make([]byte, 1024)
		n, caddr, err := ts.con.ReadFromUDP(buf)
		if err != nil {
			return err
		}
		ts.log.Debug("read %d bytes from %s\n", n, caddr)
		ts.log.Info("[%s] client connected\n", caddr)

		buf = append(make([]byte, 0), buf[:n]...)

		if err := ts.handleFirstPacket(buf, caddr); err != nil {
			ts.log.Error("[%s] %v\n", caddr, err)
		}
	}
}

func isWriteOrReadOpcode(opcode uint16) bool {
	return opcode > 0 && opcode < 3
}

func isModeSupported(mode string) error {
	loweredMode := strings.ToLower(mode)
	if !(loweredMode == "netascii" || loweredMode == "binary" || loweredMode == "octet") {
		return fmt.Errorf("mode %s not implemented", mode)
	}
	return nil
}

func (ts *Server) sendOACKifTsize(c *Client, filename string) {
	// RFC 2349
	// In Read Request packets, a size of "0" is specified in the request
	// and the size of the file, in octets, is returned in the OACK
	if _, ok := c.Options["tsize"]; ok {
		fs := ts.storage.Size(filename)
		c.log.Debug("Size of file : %d, %s\n", fs, strconv.Itoa(int(fs)))
		c.Tsize = fs
		c.Options["tsize"] = []byte(strconv.Itoa(int(fs)))
		c.doOACK()
	}
}

func (ts *Server) fileNameWithClientConfig(clientip string, filename string, opcode uint16) string {
	clientConfig := ts.ClientConfigs.Get(clientip)
	if clientConfig == nil {
		return filename
	}

	switch opcode {
	case opcodeRRQ:
		if clientConfig.ReadDir != "" {
			return clientConfig.ReadDir + "/" + filename
		}
	case opcodeWRQ:
		if clientConfig.WriteDir != "" {
			return clientConfig.WriteDir + "/" + filename
		}
	}

	return filename

}

func (ts *Server) handleFirstPacket(buf []byte, caddr *net.UDPAddr) error {

	c := newClient(caddr)
	c.log = ts.log

	if ts.IsIPBlocked(caddr.IP.String()) {
		ts.log.Info("[%s] IP %s is blocked\n", caddr, caddr.IP)
		c.sendERR(7, "no")
		c.Conn.Close()
		return nil
	}

	opcode := binary.BigEndian.Uint16(buf)
	if !isWriteOrReadOpcode(opcode) {
		c.sendERR(4, "no valid opcode")
		c.Conn.Close()
		return fmt.Errorf("no valid opcode, opcode: %d (%s)", opcode, opcodeString(opcode))
	}

	if !ts.AllowedOPCodeforClient(c.Addr.IP.String(), opcode) {
		c.sendERR(2, "requested opcode not allowed")
		c.Conn.Close()
		return fmt.Errorf("opcode: %d (%s) not allowed", opcode, opcodeString(opcode))
	}

	fields := bytes.Split(buf[2:], []byte{0x00})
	ts.log.Debug("[%s] Init Packet fields %d\n%v\n%s\n", caddr, len(fields), fields, string(buf))

	if len(buf[2:]) < 2 {
		c.sendERR(0, "malformed init packet")
		c.Conn.Close()
		return fmt.Errorf("init packet malformed, count of fields < 2")
	}
	filename := string(fields[0])
	mode := string(fields[1])

	// TODO: Santinize or check
	if !isFileNameAllowed(filename) {
		c.sendERR(2, "filename not allowed")
		c.Conn.Close()
		return fmt.Errorf("filename not allowed")
	}

	if err := isModeSupported(mode); err != nil {
		c.sendERR(0, err.Error())
		c.Conn.Close()
		return err
	}

	c.parseOptions(fields)

	ts.log.Debug("[%s] requested file: %s mode: %s opcode: %d (%s) with options: %#v\n", c.Addr, filename, mode, opcode, opcodeString(opcode), c.Options)
	filename = ts.fileNameWithClientConfig(c.Addr.IP.String(), filename, opcode)
	ts.log.Debug("[%s] filename with client %s\n", c.Addr, filename)

	switch opcode {
	case opcodeRRQ:
		ts.log.Info("[%s] RRQ from %s for file %s\n", c.Addr, c.Addr.IP, filename)

		ts.wg.Add(1)
		go ts.goRRQ(c, filename)
	case opcodeWRQ:
		ts.log.Info("[%s] WRQ from %s for file %s\n", c.Addr, c.Addr.IP, filename)

		ts.wg.Add(1)
		go ts.goWRQ(c, filename, mode)
	}

	return nil
}

func (ts *Server) goRRQ(gc *Client, filename string) {
	fr, err := ts.storage.GetFile(filename)
	if err != nil {
		gc.sendERR(1, "file not found")
		gc.Conn.Close()
		ts.log.Error("[%s] %s file not found", gc.Addr, filename)
		return
	}
	defer ts.wg.Done()

	ts.sendOACKifTsize(gc, filename)

	if err := gc.SendFile(fr); err != nil {
		ts.log.Error(" sendfile: %v\n", err)
	}
	gc.Close()
}

func (ts *Server) goWRQ(gc *Client, filename string, mode string) {
	defer ts.wg.Done()
	fw, err := ts.storage.CreateFile(filename)
	if err != nil {
		gc.sendERR(0, err.Error())
		gc.Conn.Close()
		ts.log.Error("[%s] fehler beim schreiben der Datei %s: %v\n", gc.Addr, filename, err)
		return
	}

	if err := gc.NewTransfer(mode); err != nil {
		ts.log.Error("[%s] Transfer: %v\n", gc.Addr, err)
		gc.sendERR(0, err.Error())
	}
	gc.Close()

	ts.log.Debug("[%s] client databuf %d\n", gc.Addr, len(gc.Databuf))
	wns, err := fw.Write(gc.Databuf)
	if err != nil {
		ts.log.Error("[%s] %s not written: %v\n", gc.Addr, err)
	}
	ts.log.Info("write %d bytes\n", wns)
	fw.Close()
}
