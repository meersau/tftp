[![GoDoc](https://godoc.org/bitbucket.org/meersau/tftp?status.svg)](https://godoc.org/bitbucket.org/meersau/tftp)

TFTP-Server Implentierung in Go
===============================

Gibt zwar schon einige, aber ich wollte mal ein Protokol selbst umsetzen und
dabei gleich mal Go ausprobieren.

Die Implentierung konzentriert sich auf den Server und sollte ACLs bereistellen
mit denen man eine Client IP einem Verzeichnis zuordnen kann.

Der Server funktioniert soweit. Was noch fehlt ist das Retransmit von nicht
geACKten Paketen und eine Implementierung von Timeouts.

Status: Work in Progress

 * WRQ (mode netascii entfernt \r)
 * RRQ ohne Beachtung von mide netascii
 * ACL für RRQ und WRQ
 

RFCs
----
 * RFC1350
 * RFC2348
 * RFC2348




